package main

import (
	"math/big"
)

// Types
type EncodedSumPostBodyRequest struct {
	EncryptedValues []string `json:"encryptedValues"`
}

type EncodedSumPostBodyResponse struct {
	EncryptedSum string `json:"encryptedSum"`
}

type DecodeSumPostBodyRequest struct {
	EncryptedSum string `json:"encryptedSum"`
}

type DecodeSumPostBodyResponse struct {
	PlainTextSum string `json:"plainTextSum"`
}

type EncodePostBodyRequest struct {
	Value            int      `json:"value"`
	RandomKey        string   `json:"randomKey"`
	NodeRegistration string   `json:"nodeRegistration"`
	Registrations    []string `json:"registrations"`
}

type EncodePostBodyResponse struct {
	EncodedValue string `json:"encodedValue"`
}

type Registrations struct {
	Nodes []RegistrationNode `json:"nodes"`
}

type RegistrationNode struct {
	Random       string `json:"random"`
	Registration string `json:"registration"`
}

type EncodedValues struct {
	Nodes []EncodedValueNode `json:"nodes"`
}

type EncodedValueNode struct {
	Value string `json:"encodedValue"`
}

type GP512Dataset struct {
	GP GP512 `json:"512"`
}

type GP512 struct {
	G string `json:"g"`
	P string `json:"p"`
}

type GP768Dataset struct {
	GP GP768 `json:"768"`
}

type GP768 struct {
	G string `json:"g"`
	P string `json:"p"`
}

type GP1024Dataset struct {
	GP GP1024 `json:"1024"`
}

type GP1024 struct {
	G string `json:"g"`
	P string `json:"p"`
}

type OutputResult struct {
	Size          string
	G             *big.Int
	P             *big.Int
	EncodedVals   []*big.Int
	PlainTextVals []int
}

// Functions
// create a handler struct
type HttpHandler struct {
	Size        string
	GPFile      string
	resultsFile string
}

type Person struct {
	Name string
	Age  int
}
