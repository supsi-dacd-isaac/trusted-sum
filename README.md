# Trusted Sum

Trusted Sum is a web-server providing the operations needed to implement a trusted sum based on homomorphic encryption. 

Reference: https://asecuritysite.com/encryption/go_priv

## Requirements:

`Golang >= 1.13.6`

## Executable building:

<pre>
# go build -o $GOPATH/bin/trusted-sum main.go types.go server.go functions.go
</pre>

## Available requests:

### _create_registration_

`create_registration` calculates a public registration string and a private random number.

<pre>
Request type: GET
URL: http://localhost:$PORT/create_registration
Payload: None
Returned data: Registration string
</pre>

### _encode_

_encode_ encrypts a value given a collection of registrations.

<pre>
Request type: POST
URL: http://localhost:$PORT/encode
Payload: 
        {
            "value": X,                             // Addend to encode
            "randomKey": random_key,                // Private key of the node performing the request
            "nodeRegistration": registration,       // Public registration of the node performing the request
            "registrations": meters_registrations   // Public registrations of the other nodes partecipating to the sum
        }

Returned data: Encoded value of the addend X
</pre>


### _encoded_sum_

_encoded_sum_ calculate the encoded sum given the public encoded addends.

<pre>
Request type: POST
URL: http://localhost:$PORT/encoded_sum
Payload: {"encryptedValues": enc_values}
Returned data: encryptedSum
</pre>


### _decode_sum_

_encoded_sum_ calculate the plain text sum given the encoded one.

<pre>
Request type: POST
URL: http://localhost:$PORT/decode_sum
Payload: {"EncryptedSum": enc_sum}
Returned data: Plain text sum
</pre>


## Operations sequence:

Expoiting the aforementioned requests, a set of nodes can perform a trusted sum with the following requests sequence:

<pre>
1) create_registration: Each nodes publishes the related registrations and saved their private keys.
2) encode: Each node encodes their value using the own private key and the registrations of all the nodes. The encoded values are published.
</pre>

Accessing the encoded values saved in 2), each node can calculate the encoded sum performing a `encoded_sum` request and then obtain the plaint text value with `decode_sum`.

## Acknowledgements
The authors would like to thank the Swiss Federal Office of Energy (SFOE) and the Swiss Competence Center for Energy Research - Future Swiss Electrical Infrastructure (SCCER-FURIES), for their financial and technical support to this research work.
