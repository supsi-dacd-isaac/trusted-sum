package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

func main() {

	// Command
	cmd := os.Args[1]

	// Length
	size := os.Args[2]

	if cmd == "server" {
		port := os.Args[3]

		handler := HttpHandler{size, os.Args[4], os.Args[5]}
		log.Println(fmt.Sprintf("Started server on port %s", port))

		port = fmt.Sprintf(":%s", port)
		http.ListenAndServe(port, handler)
	} else {
		g, p := getGP(size, os.Args[3])

		if cmd == "create_registration" {
			createRegistration(g, p)
		}

		if cmd == "encode" {
			idx, _ := strconv.ParseInt(os.Args[5], 10, 64)
			encode(g, p, os.Args[4], idx, os.Args[6])
		}

		if cmd == "encode_sum" {
			encodeSum(p, os.Args[4])
		}

		if cmd == "decode_sum_brute_force" {
			start, _ := strconv.ParseInt(os.Args[4], 10, 64)
			end, _ := strconv.ParseInt(os.Args[5], 10, 64)
			decodeSumBruteForce(g, p, os.Args[6], start, end)
		}

		if cmd == "decode_sum_binary_search" {
			decodeSumBinarySearch(g, p, os.Args[4], os.Args[5])
		}

		if cmd == "create_results_file" {
			end, _ := strconv.ParseInt(os.Args[4], 10, 64)
			createSortedResults(size, g, p, os.Args[5], end)
		}
	}
}
