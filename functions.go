package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/big"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func createArrayBigInt(source []string) (target []*big.Int) {
	target = make([]*big.Int, len(source))
	for i := 0; i < len(source); i++ {
		target[i], _ = new(big.Int).SetString(source[i], 10)
	}
	return target
}

func readJson(filePath string) (stream []byte) {
	// Read from JSON  file
	jsonFile, _ := os.Open(filePath)
	defer jsonFile.Close()
	stream, _ = ioutil.ReadAll(jsonFile)
	return stream
}

func getGP(size string, gpsFile string) (g, p *big.Int) {
	// Read from JSON  file
	byteValue := readJson(gpsFile)

	if size == "512" {
		var gp GP512Dataset
		json.Unmarshal([]byte(byteValue), &gp)
		p, _ = new(big.Int).SetString(gp.GP.P, 16)
		g, _ = new(big.Int).SetString(gp.GP.G, 16)
	} else if size == "768" {
		var gp GP768Dataset
		json.Unmarshal([]byte(byteValue), &gp)
		p, _ = new(big.Int).SetString(gp.GP.P, 16)
		g, _ = new(big.Int).SetString(gp.GP.G, 16)
	} else if size == "1024" {
		var gp GP1024Dataset
		json.Unmarshal([]byte(byteValue), &gp)
		p, _ = new(big.Int).SetString(gp.GP.P, 16)
		g, _ = new(big.Int).SetString(gp.GP.G, 16)
	} else {
		p, _ = new(big.Int).SetString("0", 10)
		g, _ = new(big.Int).SetString("0", 10)
	}
	return
}

func BigInt(val string) (r *big.Int) {
	r, _ = new(big.Int).SetString(val, 10)
	return
}

func makeG(g *big.Int, val string, p *big.Int) (r *big.Int) {

	v := BigInt(val)

	return (new(big.Int).Exp(g, v, p))

}

func calcit(i, n int, g, p *big.Int, y []*big.Int) (res *big.Int) {

	res, _ = new(big.Int).SetString("1", 10)

	for j := 0; j <= i-1; j++ {

		gm := y[j]
		res = new(big.Int).Mul(res, gm)
		res = new(big.Int).Mod(res, p)

	}
	for j := i + 1; j < n; j++ {

		gm := y[j]
		gm2 := new(big.Int).ModInverse(gm, p)
		res = new(big.Int).Mul(res, gm2)
		res = new(big.Int).Mod(res, p)

	}
	fmt.Printf(" ")
	return new(big.Int).Mod(res, p)

}

func getEncodedValue(Y *big.Int, x string, p, V *big.Int) (r *big.Int) {
	r = new(big.Int).Mul(new(big.Int).Exp(Y, BigInt(x), p), V)
	return r
}

func mult(n int, val []*big.Int, p *big.Int) (r *big.Int) {
	res := val[0]

	for j := 1; j < n; j++ {
		res = new(big.Int).Mul(res, val[j])
	}
	return (new(big.Int).Mod(res, p))

}

func createRegistration(g, p *big.Int) (string, *big.Int) {
	registration := big.NewInt(0)

	// Define the seed
	rand.Seed(time.Now().UnixNano())
	rand := strconv.Itoa(rand.Intn(1e9))

	registration = makeG(g, rand, p)
	fmt.Println(fmt.Sprintf("Random=%s", rand))
	fmt.Println(fmt.Sprintf("Registration=%d", registration))
	return rand, registration
}

func encode(g, p *big.Int, value string, meterIdx int64, regsFile string) {
	// Read from JSON  file
	byteValue := readJson(regsFile)

	// Initialize the nodes array
	var regNodes Registrations
	json.Unmarshal(byteValue, &regNodes)

	// Create the registrations array
	registrations := make([]*big.Int, len(regNodes.Nodes))
	for i := 0; i < len(regNodes.Nodes); i++ {
		registrations[i] = big.NewInt(0)
		registrations[i].SetString(regNodes.Nodes[i].Registration, 10)
	}

	// Encode the value
	V := makeG(g, value, p)
	Y := calcit(int(meterIdx), len(regNodes.Nodes), g, p, registrations)
	encY := getEncodedValue(Y, regNodes.Nodes[int(meterIdx)].Random, p, V)

	fmt.Println(fmt.Sprintf("EncY=%d", encY))
}

func encodeSum(p *big.Int, encodedValuesFile string) {

	// Read from JSON  file
	byteValue := readJson(encodedValuesFile)

	// Initialize the nodes array
	var encValsNodes EncodedValues
	json.Unmarshal(byteValue, &encValsNodes)

	// Create the encoded values array
	encVals := make([]*big.Int, len(encValsNodes.Nodes))
	for i := 0; i < len(encValsNodes.Nodes); i++ {
		encVals[i] = big.NewInt(0)
		encVals[i].SetString(encValsNodes.Nodes[i].Value, 10)
	}

	encSum := mult(len(encValsNodes.Nodes), encVals, p)
	fmt.Println(fmt.Sprintf("EncSum=%d", encSum))
}

func decodeSumBruteForce(g, p *big.Int, encSumStr string, start, end int64) {
	encSum := big.NewInt(0)
	encSum.SetString(encSumStr, 10)

	var flagFound bool = false
	for i := int(start); i <= int(end); i++ {
		m, _ := new(big.Int).SetString(strconv.Itoa(i), 10)

		gm := new(big.Int).Exp(g, m, p)

		if gm.Cmp(encSum) == 0 {
			fmt.Println(fmt.Sprintf("Sum=%d", i))
			flagFound = true
			break
		}
	}

	if !flagFound {
		fmt.Println(fmt.Sprintf("Sum=NotFound"))
	}
}

func decodeSumBinarySearch(g, p *big.Int, resultsFile string, encSumStr string) {
	encSum := big.NewInt(0)
	encSum.SetString(encSumStr, 10)

	// Read from JSON  file
	byteValue := readJson(resultsFile)

	// Initialize the data array
	var dataset OutputResult
	json.Unmarshal([]byte(byteValue), &dataset)

	idx := binarySearch(encSum, dataset.EncodedVals)

	if idx == -1 {
		fmt.Println(fmt.Sprintf("Sum=NotFound"))
	} else {
		fmt.Println(fmt.Sprintf("Sum=%d", dataset.PlainTextVals[idx]))
	}
}

func binarySearch(needle *big.Int, haystack []*big.Int) int {

	low := 0
	high := len(haystack) - 1

	for low <= high {
		median := (low + high) / 2

		//if haystack[median] < needle {
		if haystack[median].Cmp(needle) == -1 {
			low = median + 1
		} else {
			high = median - 1
		}
	}

	if low == len(haystack) || haystack[low] != needle {
		return low
	}

	return -1
}

func createSortedResults(size string, g, p *big.Int, resultsFile string, end int64) {

	gms := make([]*big.Int, end+1)
	idxsMap := make(map[*big.Int]int)

	for i := 0; i <= int(end); i++ {
		m, _ := new(big.Int).SetString(strconv.Itoa(i), 10)
		gms[i] = new(big.Int).Exp(g, m, p)
		idxsMap[gms[i]] = i
	}

	// Sort the results
	sortedGms := quicksort(gms)
	writeOnResultsFile(size, g, p, sortedGms, resultsFile, idxsMap)
}

func writeOnResultsFile(size string, g, p *big.Int, results []*big.Int, outputsFile string, idxsMap map[*big.Int]int) {
	idxsList := make([]int, len(results))
	for i := 0; i < len(results); i++ {
		idxsList[i] = idxsMap[results[i]]
	}

	jsonData := &OutputResult{Size: size, G: g, P: p, EncodedVals: results, PlainTextVals: idxsList}
	encJson, _ := json.MarshalIndent(jsonData, "", " ")

	_ = ioutil.WriteFile(outputsFile, encJson, 0644)
}

func quicksort(a []*big.Int) []*big.Int {
	if len(a) < 2 {
		return a
	}

	left, right := 0, len(a)-1

	pivot := rand.Int() % len(a)

	a[pivot], a[right] = a[right], a[pivot]

	for i, _ := range a {
		//if a[i] < a[right] {
		if a[i].Cmp(a[right]) == -1 {
			a[left], a[i] = a[i], a[left]
			left++
		}
	}

	a[left], a[right] = a[right], a[left]

	quicksort(a[:left])
	quicksort(a[left+1:])

	return a
}
