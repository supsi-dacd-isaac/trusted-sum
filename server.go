package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"math/rand"
	"net/http"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

func (h HttpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	log.Println(fmt.Sprintf("%s %s%s", r.Proto, r.Host, r.URL.Path))

	g, p := getGP(h.Size, h.GPFile)

	if r.URL.Path[1:] == "create_registration" && r.Method == "GET" {
		// Create the random number and the registration
		rand.Seed(time.Now().UnixNano())
		rand := strconv.Itoa(rand.Intn(1e9))
		registration := makeG(g, rand, p)

		// Encode the response in JSON format
		jsonData := &RegistrationNode{Random: rand, Registration: registration.String()}
		encJson, _ := json.MarshalIndent(jsonData, "", "")
		fmt.Fprintf(w, string(encJson))

	} else if r.URL.Path[1:] == "encode" && r.Method == "POST" {
		// Decode the request body
		var bodyDatasetRequest EncodePostBodyRequest
		err := json.NewDecoder(r.Body).Decode(&bodyDatasetRequest)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// Create the registrations array
		registrations := createArrayBigInt(bodyDatasetRequest.Registrations)

		// Get the right id
		nodeRegistration, _ := new(big.Int).SetString(bodyDatasetRequest.NodeRegistration, 10)
		idxRegistration := binarySearch(nodeRegistration, quicksort(registrations))

		// Encrypt the value
		V := makeG(g, strconv.Itoa(bodyDatasetRequest.Value), p)
		Y := calcit(idxRegistration, len(bodyDatasetRequest.Registrations), g, p, registrations)
		encY := getEncodedValue(Y, bodyDatasetRequest.RandomKey, p, V)

		// Encode the response in JSON format
		jsonData := &EncodePostBodyResponse{EncodedValue: encY.String()}
		encJson, _ := json.MarshalIndent(jsonData, "", "")
		fmt.Fprintf(w, string(encJson))

	} else if r.URL.Path[1:] == "encoded_sum" && r.Method == "POST" {
		// Decode the request body
		var bodyDatasetRequest EncodedSumPostBodyRequest
		err := json.NewDecoder(r.Body).Decode(&bodyDatasetRequest)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// Create the registrations array
		encryptedValues := createArrayBigInt(bodyDatasetRequest.EncryptedValues)

		// Encrypt the sum
		encSum := mult(len(bodyDatasetRequest.EncryptedValues), quicksort(encryptedValues), p)

		// Encode the response in JSON format
		jsonData := &EncodedSumPostBodyResponse{EncryptedSum: encSum.String()}
		encJson, _ := json.MarshalIndent(jsonData, "", "")
		fmt.Fprintf(w, string(encJson))

	} else if r.URL.Path[1:] == "decode_sum" && r.Method == "POST" {

		// Decode the request body
		var bodyDatasetRequest DecodeSumPostBodyRequest
		err := json.NewDecoder(r.Body).Decode(&bodyDatasetRequest)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		encSum, _ := new(big.Int).SetString(bodyDatasetRequest.EncryptedSum, 10)

		// Launch external command to prevent huge usage of memory
		cmd := exec.Command("./trusted-sum", "decode_sum_binary_search", h.Size, h.GPFile, h.resultsFile, encSum.String())
		out, err := cmd.Output()
		if err != nil {
			panic(err)
		}
		plainTextSum := strings.Split(string(out), "=")

		// Encode the response in JSON format
		jsonData := &DecodeSumPostBodyResponse{PlainTextSum: strings.Trim(plainTextSum[1], "\n")}
		encJson, _ := json.MarshalIndent(jsonData, "", "")
		fmt.Fprintf(w, string(encJson))
	} else {
		log.Println(fmt.Sprintf("Service %s not available", r.URL.Path[1:]))
		http.Error(w, fmt.Sprintf("Service %s not available", r.URL.Path[1:]), http.StatusInternalServerError)
	}
}
